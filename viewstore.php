<?php
  $page1 = "menu";
  $page = "store";
  include "includes/header.php"; ?>
        <div class="span9">
        	<!--form class="form-search"  style ="float:right">
  				<div class="input-append">
    				<input type="text" class="span9 search-query" id="items" onClick="javascript:suggest(this,'store','item');">
    				<button type="button" class="btn" id="search" onClick="javascript:searchstore();">Search</button>
  				</div>
			     </form-->
          <div class="input-append">
            <input type="text" class="span9 search-query" id="search" placeholder="Type to search">
          </div>
        	<table  class="table table-hover" id="items-table">
        		<tr>
        			<th>#</th>
        			<th>Item</th>
        			<th>Stock</th>
        			<th>Rate</th>
        			<th>Rate Per</th>
        		</tr>
        		<?php
        			include "db.php";
              $i=0;
        			$result = mysql_query("SELECT * FROM store");
        			while($row = mysql_fetch_array($result)){
        		?>
        		<tr id='item-<?php echo $row[0] ?>'>
        			<td><?php echo $i + 1 ?></td>
        			<td> <a href="#" id="pencil-<?php echo $row[0] ?>"><?php echo $row[1] ?></a></td>
              <td> <a href="#" id="pencil1-<?php echo $row[0] ?>"><?php echo $row[2] ?></a></td>
              <td> <a href="#" id="pencil2-<?php echo $row[0] ?>"><?php echo $row[3] ?></a></td>
        			<td><?php echo $row[4] ?></td>
            <td><button class="btn btn-primary btn-small" onClick="javascript:editstore(<?php echo $row[0]?>);" name="edits"><i class="icon-pencil icon-white"></i></button>
    				<button class="btn btn-danger btn-small" onClick="javascript:deletestore(<?php echo $row[0]?>);"><i class="icon-remove icon-white"></i></button></td>
        		</tr>
        		<?php
                $i +=1;		
        			}
        		?>
        	</table>

        </div>
  <?php include "includes/footer.php"; ?>
  <script type="text/javascript">
    var $rows = $('#items-table tr');
    $('#search').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
    });
  </script>
  <script type="text/javascript">

      function editstore(id){
        $(document).ready(helper(id));
      }

      function helper(id){
        return function() {
          $.fn.editable.defaults.mode = 'inline';
            $('#pencil-'+id).editable({
              type: 'text',
              success: function(response, newValue) {
              //userModel.set('username', newValue); //update backbone model
              console.log(newValue);
              $.post("ajax/editing.php", {
              tablename: 'store', columnname: 'item', value: newValue, col: id
              },
              function(data){
                console.log(data);
              });
              }
            });
            $('#pencil1-'+id).editable({
              type: 'text',
              success: function(response, newValue) {
              //userModel.set('username', newValue); //update backbone model
              console.log(newValue);
              $.post("ajax/editing.php", {
              tablename: 'store', columnname: 'instock', value: newValue, col: id
              },
              function(data){
                console.log(data);
              });
              }
            });
            $('#pencil2-'+id).editable({
              type: 'text',
              success: function(response, newValue) {
              //userModel.set('username', newValue); //update backbone model
              console.log(newValue);
              $.post("ajax/editing.php", {
              tablename: 'store', columnname: 'rate', value: newValue, col: id
              },
              function(data){
                console.log(data);
              });
              }
            });
          }
        }
  </script>
  <script type="text/javascript">
  	function deletestore(id) {
      if(confirm("Are you sure?")){
        //window.location = "viewtrans.php?del=" + id +"&customer="+ cname;
        var url = "ajax/delete_trans.php";
        $.post(url,{
        	table: 'store' , val : id 
        },function(data){
          $("#item-"+id).remove(); 
        });
      }  
    };
  </script>
   <script src="js/pareek.js"></script>
   <script src="js/bootstrap-editable.min.js"></script>