<?php
  $page1 = "menu";
  $page = "customerlist";
  include "includes/header.php"; 
  ?>
<div class="span9">
	          <div class="input-append">
            <input type="text" class="span9 search-query" id="search" placeholder="Type to search">
          </div>
        	<table  class="table table-hover" id="items-table">
        		<tr>
        			<th>#</th>
        			<th>Name</th>
        			<th>Father's Name</th>
        			<th>Phone</th>
        		</tr>
        		<?php
        			include "db.php";
        			$result = mysql_query("SELECT * FROM customers");
        			$i=0;
        			while($row = mysql_fetch_array($result)){
        		?>
        		<tr id='cust-<?php echo $row[0] ?>'>
        			<td><?php echo $i + 1 ?></td>
        			<td> <a href="#" id="pencil-<?php echo $row[0] ?>"><?php echo $row[1] ?></a></td>
        			<td> <a href="#" id="pencil1-<?php echo $row[0] ?>"><?php echo $row[2] ?></a></td>
        			<td> <a href="#" id="pencil2-<?php echo $row[0] ?>"><?php echo $row[3] ?></a></td>
    				<td><button class="btn btn-primary btn-small" onClick="javascript:editcustomer(<?php echo $row[0]?>);" name="edits"><i class="icon-pencil icon-white"></i></button>
              <button class="btn btn-danger btn-small" onClick="javascript:deletestore(<?php echo $row[0]?>);"><i class="icon-remove icon-white"></i></button></td>
        		</tr>
        		<?php
        				$i +=1;		
        			}
        		?>
        	</table>

        </div>
  <?php include "includes/footer.php"; ?>
  <script type="text/javascript">
    var $rows = $('#items-table tr');
    $('#search').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
    });
  </script>

  <script type="text/javascript">
  	
    function deletestore(id) {
      if(confirm("Are you sure?")){
        //window.location = "viewtrans.php?del=" + id +"&customer="+ cname;
        var url = "ajax/delete_trans.php";
        $.post(url,{
        	table: 'customers' , val : id 
        },function(data){
          $("#cust-"+id).remove(); 
        });
      }  
    };
  </script>


  <script type="text/javascript">

      function editcustomer(id){
        $(document).ready(helper(id));
      }

      function helper(id){
        return function() {
          $.fn.editable.defaults.mode = 'inline';
            $('#pencil-'+id).editable({
              type: 'text',
              success: function(response, newValue) {
              //userModel.set('username', newValue); //update backbone model
              console.log(newValue);
              $.post("ajax/editing.php", {
              tablename: 'customers', columnname: 'name', value: newValue, col: id
              },
              function(data){
                console.log(data);
              });
              }
            });
            $('#pencil1-'+id).editable({
              type: 'text',
              success: function(response, newValue) {
              //userModel.set('username', newValue); //update backbone model
              console.log(newValue);
              $.post("ajax/editing.php", {
              tablename: 'customers', columnname: 'fathername', value: newValue, col: id
              },
              function(data){
                console.log(data);
              });
              }
            });
            $('#pencil2-'+id).editable({
              type: 'text',
              success: function(response, newValue) {
              //userModel.set('username', newValue); //update backbone model
              console.log(newValue);
              $.post("ajax/editing.php", {
              tablename: 'customers', columnname: 'phone', value: newValue, col: id
              },
              function(data){
                console.log(data);
              });
              }
            });
          }
        }
  </script>
   <script src="js/pareek.js"></script>
   <script src="js/bootstrap-editable.min.js"></script>