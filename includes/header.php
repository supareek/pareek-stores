<?php
  session_start();
    if(!isset($_SESSION['user']))
    {
      header('Location: login.php');
      exit();
    }
    else{
      $name = $_SESSION['user'];      
    }
  ?>
<html>
<head>
	<title> Dashboard </title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/invoice.css" rel="stylesheet">
    <link href="css/bootstrap-editable.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>
<body>
	
	 <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="dashboard.php">Pareek Stores</a>
          <div class="nav-collapse collapse">
            <ul class="nav pull-right">
             <li> <a href="dashboard.php" ><?php echo $name ?></a></li>
             <li><a href="logout.php">Logout</a></li>
            </ul>
            <ul class="nav">
              <li class=<?php if($page1 == "menu"){  echo "active"; } ?> ><a href="dashboard.php"><i class="icon-home"></i> Menu</a></li>
              <li class=<?php if($page1 == "about"){  echo "active"; } ?>><a href="about.php">About</a></li>
              <li class=<?php if($page1 == "contact"){  echo "active"; } ?> ><a href="contact.php">Contact</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

<!-- Closing tag in footer.php -->
      <div class="container-fluid">

          <div class="row-fluid">
       <?php include "includes/sidebar.php"; ?>