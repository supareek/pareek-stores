 <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Dashboard</li>
              <li class=<?php if($page == "dashboard"){  echo "active"; } ?> ><a href="dashboard.php">Home</a></li>
              <li class="nav-header">Transaction Related</li>
              <li class=<?php if($page == "addtrans"){ echo "active";}  ?> ><a href="addtrans.php">Add Transaction</a></li>
              <li class=<?php if($page == "viewtrans"){ echo "active";} ?> ><a href="viewtrans.php">View Transaction</a></li>
              <li class="nav-header">Customer</li>
              <li class=<?php if($page == "addcustomer"){ echo "active";} ?> ><a href="addcustomer.php">Add Customer</a></li>
              <li class=<?php if($page == "settlecustomer"){ echo "active";} ?> ><a href="settlecustomer.php">Settle Customer</a></li>
              <li class=<?php if($page == "customerlist"){ echo "active"; } ?> ><a href="viewcustomer.php">Customers List</a></li>
              <li class="nav-header">Inventory</li>
              <li class=<?php if($page == "additem"){ echo "active";} ?> ><a href="additem.php">Add Item</a></li>
              <li class=<?php if($page == "store"){ echo "active";} ?> ><a href="viewstore.php">View Store</a></li>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->