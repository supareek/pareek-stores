<?php
  $page1 = "menu";
  $page = "addtrans";
  include "includes/header.php"; 
?>
<div class="span9">
  <form class="form-horizontal"  id="main" method="post" action="genbill.php" >
  <fieldset>
    <legend>Add New Transaction</legend>
    <!--div class="control-group"> 
      <label class="control-label" for="inputEmail">Customer Name</label>
      <div class="controls">
        <input type="text" id="search" placeholder="Required" name="name" onClick="javascript:suggest(this,'customers','name');">
      </div>
    </div-->
    <table  class="table table-hover" id="item-table" name="itemtable">
      <tbody>
      <tr>
        <th>Item</th>
        <th>Quantity</th>
        <th>Rate</th>
        <th>Amount</th>
      </tr>
      <tr id="editable">
        <td><input type="text" id="itemName" placeholder="Required" name="itemname[]" onClick="javascript:suggest(this,'store','item');" onchange="javascript:updateRate(this)" required></td>
        <td><input type="text" class = "numeric" id="quantity" placeholder="Required" name="itemqty[]" onchange="javascript:updateAmount(this)" required></td>
        <td><input type="text" id="rate" readonly="readonly"  name="itemrate[]"></td>
        <td><input type="text" id="amount" readonly="readonly" name="itemamt[]"></td>
        <td id="remove" style="display: none"><button class="btn btn-danger btn-small" type="button" onClick="javascript:deleteitem(this);"><i class="icon-remove icon-white"></i></button></td>
      </tr>
    </tbody>
    </table>
      <div class = "control-group">
        <button name="addbutton" type="button" id="additem" class="btn btn-success btn-small" onClick="javascript:addItem();">Add Item</button>
        <!--button type="button" id ="done" class="btn btn-primary btn-small" onClick="javascript:updateTotal(addbutton)">Done</button-->
      </div>     
     <div class="control-group">
      <div class="controls">
        <label class="radio">
          <input type="radio" name="full" id="optionsRadios1" value="option1" checked>
            Full Payment
        </label>
        <label class="radio">
          <input type="radio" name="full" id="optionsRadios2" value="option2">
            Partial Payment
        </label>
      </div>
    </div>
    <div class="control-group">
          <table  class="table table-hover" id="payment-table">
              <tr id="payment" style="display: none;">
                <td><label> Customer Name </label><input type="text" id="cname" placeholder="Required" onClick="javascript:suggest(this,'customers','name');"></td>
                <td><label> Paid Amount </label><input type="text" class="numeric" id="paidamount" placeholder="Required"></td>
                <td style="display: none;"><label> Due Amount </label><input type="text" class="numeric" readonly="readonly" id="dueamount" placeholder="Required"></td>
              </tr>
          </table>
    </div>
    <div class = "control-group">
      <div class="controls">
        <label class="control-label"><h2>Total : </h2></label>
        <h2 id="upd"></h2> 
      </div>
        <input type="hidden" id="totalamt" name="totalamt" value="0">
    </div> 
    <div class="control-group">
        <button type="submit" class="btn btn-primary" name="generate">Generate Bill</button>
    </div>
  </fieldset>
</form>

</div>
<?php include "includes/footer.php"; ?>

<!-- Disable form submit on pressing Enter key -->

<script type="text/javascript">
        $(document).ready(function() {
           $("form").bind("keypress", function(e) {
              if (e.keyCode == 13) {
                 return false;
              }
           });
        });
</script>

<script type="text/javascript">
        $(document).ready(function(){
          console.log("inside the method ");
          $(".numeric").numeric();
        });
        
</script>
<script type="text/javascript">

        $('#paidamount').change(function (e){
          var row1 = document.getElementById('payment');
          var inp1 = row1.cells[1].getElementsByTagName('input')[0];
          var val = document.getElementById('totalamt').value;
          var col = row1.cells[2];
          col.removeAttribute("style");
          var inp = row1.cells[2].getElementsByTagName('input')[0];
          inp.value = val - inp1.value;
        });

        $('#optionsRadios1').click(function(e) {
          var row1 = document.getElementById('payment');
          var inp = row1.cells[0].getElementsByTagName('input')[0];
          inp.name = '';
          inp.value ='';
          inp.required = false;

          var inp2 = row1.cells[1].getElementsByTagName('input')[0];
          inp2.name = '';
          inp2.value = '';
          inp2.required = false;

          var inp3 = row1.cells[2].getElementsByTagName('input')[0];
          inp3.value ='';

          row1.style.visibility = 'hidden';
           $("#optionsRadios2").prop("disabled",false);
        });
        $('#optionsRadios2').click(function(e) {

          var row1 = document.getElementById('payment');
          row1.removeAttribute("style");
          var inp = row1.cells[0].getElementsByTagName('input')[0];
          inp.name = 'custname';
          inp.required = true;

          var inp2 = row1.cells[1].getElementsByTagName('input')[0];
          inp2.name = 'paidamt';
          inp2.required = true;

        /*$('#newbox').html("<input type='text' id='cname' placeholder='Required' name='name' onClick='javascript:suggest(this,'customers','name');'>");  
          $('#newbox1').append("<input type='text' id='pamt' placeholder='Required' name='name'>"); 
          $("#optionsRadios2").prop("disabled",true);
          var tab = document.getElementById('payment-table');
          tab.style.visibility = 'visible';
          var new_row = tab.rows[0].cloneNode(true);
          var len = tab.rows.length;
          new_row.removeAttribute("style");
          new_row.id +=len;
          var inp2 = new_row.cells[0].getElementsByTagName('input')[0];
          inp2.id +=len;
          inp2.name +=len;
          inp2.value = '';
          inp2.placeholder = "Customer Name";
          inp2.required = true;
          tab.appendChild(new_row);
          new_row = tab.rows[0].cloneNode(true);
          len = tab.rows.length;
          new_row.removeAttribute("style");
          new_row.id +=len;
          inp2 = new_row.cells[0].getElementsByTagName('input')[0];
          inp2.removeAttribute("onClick");
          inp2.id +=len;
          inp2.name='paidamt' 
          inp2.value = '';
          inp2.placeholder = "Settled Amount";
          inp2.required = true;
          tab.appendChild(new_row);
           $("#optionsRadios2").prop("disabled",true);*/

        });
</script>

<!-- JavaScript methods to handle on click events -->

 <script type="text/javascript">

  /* Method to add rows dynamically to the table */
    function addItem(){
      var tab = document.getElementById('item-table');
      tab.rows[1].cells[4].removeAttribute('style');
      var new_row = tab.rows[1].cloneNode(true);
      var len = tab.rows.length;

      new_row.id +=len;
      //console.log(new_row.id);
      var inp1 = new_row.cells[0].getElementsByTagName('input')[0];
      inp1.id +=len;
      inp1.value ='';
      //console.log(inp1.id);

      var inp2 = new_row.cells[1].getElementsByTagName('input')[0];
      inp2.id +=len;
      inp2.value ='';
      //console.log(inp2.id);

      var inp3 = new_row.cells[2].getElementsByTagName('input')[0];
      inp3.id +=len;
      inp3.value ='';
      //console.log(inp3.id);

      var inp4 = new_row.cells[3].getElementsByTagName('input')[0];
      inp4.id +=len;
      inp4.value ='';
      //console.log(inp4.id);

      var inp5 = new_row.cells[4];
      inp5.removeAttribute('style');
      inp5.id +=len;

      tab.appendChild(new_row);
      $(".numeric").numeric();
      //$("#item-table tbody>tr:last").clone(true).insertAfter("#item-table tbody>tr:last");
    }

  /* Method to update total after entering items */
    function updateTotal(){
      var tab = document.getElementById('item-table');
      var len = tab.rows.length;
      var total = 0;
      for (var i = 1; i < len; i++) {
          total += parseFloat(tab.rows[i].cells[3].getElementsByTagName('input')[0].value);
      }
      $('#upd').html(total);
      var val = document.getElementById('totalamt');
      val.value = total;
      /*var new_row = tab.rows[1].cloneNode(true);
      var inp1 = new_row.cells[0].getElementsByTagName('input')[0];
      inp1.remove();
      //inp1.value ='';
      //console.log(inp1.id);

      var inp2 = new_row.cells[1].getElementsByTagName('input')[0];
      inp2.remove();
      //inp2.value ='';
      //console.log(inp2.id);

      var inp3 = new_row.cells[2].getElementsByTagName('input')[0];
      inp3.id +=len;
      inp3.value ='Total';
      //console.log(inp3.id);

      var inp4 = new_row.cells[3].getElementsByTagName('input')[0];
      inp4.id +=len;
      inp4.value =total;
      tab.appendChild(new_row);*/

    }

  /* Method to update fields dynamically*/
    function updateRate(val){
      var row = val.parentNode.parentNode;
      var col = row.cells[2].getElementsByTagName('input')[0].id;
      var cvalue = val.value;
      //console.log(col);
      $.post("ajax/update_rate.php",{
        itemname: cvalue
      }, function(data){
          $('#'+col).val(data);
          updateAmount(row.cells[1].getElementsByTagName('input')[0]);
      });
    }

    function updateAmount(val){
      console.log(val);
      var row = val.parentNode.parentNode;
      var col = row.cells[2].getElementsByTagName('input')[0].value;
      var col1 = row.cells[3].getElementsByTagName('input')[0].id;
      var cvalue = val.value;
      console.log(cvalue);
      var amt = cvalue * col;
      //console.log(amt);
      $('#'+col1).val(amt);
      updateTotal();
       $("#optionsRadios1").click();
    }

    function deleteitem(column){
      var tr = column.parentNode.parentNode;
      var id = tr.getAttribute('id');
      $('#'+id).remove();
      updateTotal();
      var tab = document.getElementById('item-table');
      var len = tab.rows.length;
      if(len == 2){
        tab.rows[1].cells[4].setAttribute('style','display:none');
      }

    }
 </script>

  <script src="js/pareek.js"></script>