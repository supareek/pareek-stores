-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2013 at 02:40 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `generalstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `FatherName` varchar(100) DEFAULT NULL,
  `Phone` bigint(12) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`ID`, `Name`, `FatherName`, `Phone`) VALUES
(5, 'Sanjay', 'Gopal', 9739520544),
(6, 'Sumit', 'Santosh', 9739520544),
(7, 'Syed', '', 0),
(8, 'Amit', '', 0),
(9, 'Santosh', 'NAurang', 9739520544);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE IF NOT EXISTS `store` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item` varchar(100) NOT NULL,
  `InStock` int(11) NOT NULL,
  `Rate` int(11) NOT NULL,
  `RatePer` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`ID`, `Item`, `InStock`, `Rate`, `RatePer`) VALUES
(1, 'Sugar', 100, 52, 'per 1 kg'),
(2, 'Coffee', 150, 38, 'per 200 gm'),
(3, 'Agarbati', 350, 30, 'per 100 gm pack');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedDate` timestamp NULL DEFAULT NULL,
  `Details` varchar(500) DEFAULT NULL,
  `TotalAmount` bigint(20) NOT NULL,
  `DueAmount` int(10) NOT NULL COMMENT 'Due or Settled Amount',
  `CustomerID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CustomerID` (`CustomerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`ID`, `CreatedDate`, `ModifiedDate`, `Details`, `TotalAmount`, `DueAmount`, `CustomerID`) VALUES
(7, '2013-07-18 05:34:25', '2013-07-29 08:25:33', 'Tea -10 kg', 0, 198, 5),
(17, '2013-07-23 17:02:08', '2013-08-25 05:59:03', 'Sugar - 2kg', 3000, 2800, 5),
(20, '2013-07-23 17:33:27', '2013-07-23 17:33:27', 'Settlement', 0, -980, 5),
(21, '2013-07-27 07:23:37', '2013-07-27 07:24:03', 'Ghee-1ltr', 0, 560, 5),
(33, '2013-07-28 10:00:58', '2013-07-28 10:01:23', 'Kellogs', 0, 500, 5),
(36, '2013-08-01 17:07:32', '2013-08-01 17:07:32', 'Settlement', 0, -210, 5),
(37, '2013-08-08 02:12:27', '2013-08-08 02:12:27', 'Settlement', 0, -200, 5),
(38, '2013-08-13 04:21:10', '2013-08-13 04:21:10', NULL, 0, 100, 6),
(39, '2013-08-13 04:22:16', '2013-08-13 04:22:16', 'Settlement', 0, -500, 5),
(40, '2013-08-29 17:42:22', '2013-08-29 17:42:57', 'Sugat', 1200, 500, 5);

-- --------------------------------------------------------

--
-- Table structure for table `transitem`
--

CREATE TABLE IF NOT EXISTS `transitem` (
  `TransID` int(11) NOT NULL,
  `ItemID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  KEY `TransID` (`TransID`),
  KEY `ItemID` (`ItemID`),
  KEY `ItemID_2` (`ItemID`),
  KEY `TransID_2` (`TransID`),
  KEY `ItemID_3` (`ItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unames`
--

CREATE TABLE IF NOT EXISTS `unames` (
  `uname` varchar(20) DEFAULT NULL,
  `passwd` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unames`
--

INSERT INTO `unames` (`uname`, `passwd`) VALUES
('sanjay', 'pareek'),
('satya', 'karnati');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`CustomerID`) REFERENCES `customers` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transitem`
--
ALTER TABLE `transitem`
  ADD CONSTRAINT `transitem_ibfk_2` FOREIGN KEY (`ItemID`) REFERENCES `store` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transitem_ibfk_1` FOREIGN KEY (`TransID`) REFERENCES `transaction` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
