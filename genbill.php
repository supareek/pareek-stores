<?php
  $page1 = "menu";
  $page = "genbill";
  include "includes/header.php"; 
  include "db.php";
  if(isset($_POST['generate'])){
	$itemname = $_POST['itemname'];
	$itemqty = $_POST['itemqty'];
  $itemrate = $_POST['itemrate'];
  $itemamt = $_POST['itemamt'];
  $amt = $_POST['totalamt'];
  mysql_query("SET AUTOCOMMIT=0");
  mysql_query("START TRANSACTION");
  $cnt = mysql_query("SELECT cnt FROM hits") or die(mysql_error());
  $val = mysql_fetch_array($cnt);
  $count = $val[0];
  $r1 = mysql_query("UPDATE hits SET cnt = cnt + 1")  or die(mysql_error());
	if(isset($_POST['custname'])){
		$name = $_POST['custname'];
		$settledamt = $_POST['paidamt'];
    $result = mysql_query("SELECT ID FROM customers WHERE name = '$name'")or die(mysql_error());
    $row = mysql_fetch_array($result);
    $id = $row[0];
    $due = $amt - $settledamt;
    $r2 = mysql_query("INSERT INTO transaction(id,totalamount,dueamount,customerid) VALUES(NULL,$amt,$due,$id)")or die(mysql_error());
    $r = mysql_query("SELECT max(id) FROM transaction")or die(mysql_error());
    $rows = mysql_fetch_array($r);
    $tid = $rows[0];
    foreach( $itemname as $key => $n ) {
      $res = mysql_query("SELECT instock,id FROM store WHERE item = '$n'")or die(mysql_error());
      $row = mysql_fetch_array($res);
      $iqty = $itemqty[$key];
      $qty = $row[0] - $itemqty[$key];
      $iid = $row[1];
      $r3 = mysql_query("UPDATE store SET instock = $qty WHERE item = '$n'")or die(mysql_error());
      $r4 = mysql_query("INSERT INTO transitem(transid,itemid,quantity) VALUES($tid,$iid,$iqty)")or die(mysql_error());
    }
    if ($r1 and $r2 and $r3 and $r4) {
      mysql_query("COMMIT");
    } else {        
      mysql_query("ROLLBACK");
    }
	}
	else{
    foreach( $itemname as $key => $n ) {
      //$res = mysql_query("SELECT instock FROM store WHERE item= '$n'")or die(mysql_error());
      //$row = mysql_fetch_array($res);
      //echo "<h1>".$itemqty[$key]."</h1>";
      $qty = $itemqty[$key];
      $result = mysql_query("UPDATE store SET instock = instock - $qty WHERE item = '$n'")or die(mysql_error());
    }
    if ($r1 and $result) {
    mysql_query("COMMIT");
  } else {        
    mysql_query("ROLLBACK");
  }
  }
//print $name."  ".$settledamt."  ".$amt;
  
if(isset($connect)){
      mysql_close($connect);
    }
  ?>
  <div class="span9">
        
          <!-- Page header -->
          <article class="page-header">
            <h1>Pareek Stores Invoice #<?php echo $count ?></h1>
          </article>
          <!-- /Page header -->

          <!-- Grid row -->
          <div class="row">
            
            <!-- Data block -->
            <article class="span12 data-block">
              <div class="data-container">
                <header>
                  <h2>Invoice</h2>
                  <ul class="data-header-actions">
                    <li><a href="#" class="btn btn-alt btn-inverse">Add invoice</a></li>
                    <li><a href="#" class="btn btn-alt btn-inverse">Print</a></li>
                  </ul>
                </header>

                <section>

                  <!-- Grid row -->
                  <div class="row-fluid">
                    <div class="span4">
                      <dl class="dl-horizontal">
                        <dt>Customer Name:</dt>
                        <dd><?php if(isset($_POST['custname'])){ echo $name; } else echo "Anonymus";  ?></dd>
                        <dt>Invoice Number:</dt>
                        <dd>#<?php echo $count ?></dd>
                        <dt>Invoice Date:</dt>
                        <dd><?php echo date("m/d/Y h:i:s a", time());?></dd>
                      </dl>
                    </div>
                    <div class="span4 offset4">
                      <div class="well small">
                      <h4>Shop address</h4>
                        <dl class="dl-horizontal">
                          <dt>Name:</dt>
                          <dd>Sanjay Pareek</dd>
                          <dt>Address:</dt>
                          <dd>Pareek Stores</dd>
                          <dd>Fatehpur, India 332301</dd>
                        </dl>
                      </div>
                    </div>
                  </div>
                  <!-- /Grid row -->

                  <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Item</th>
                        <th>Rate</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        foreach( $itemname as $key => $n ) {
                      ?>
                      <tr>
                        <td><?php echo ($key +1) ?></td>
                        <td><?php echo  $n ?></td>
                        <td><?php echo $itemrate[$key] ?></td>
                        <td><?php echo $itemqty[$key] ?></td>
                        <td><?php echo $itemamt[$key] ?></td> 
                      <?php        
                        }
                      ?>
                    </tbody>
                  </table>

                  <!-- Total price table -->
                  <div class="row-fluid">
                    <div class="span4 offset8">
                      <table class="table table-bordered table-striped">
                        <tr>
                          <td>Paid Amount:</td>
                          <td>Rs. <?php if(isset($_POST['custname'])){ echo $settledamt; } else echo $amt; ?></td>
                        </tr>
                        <tr>
                          <td>Due Amount:</td>
                          <td>Rs. <?php if(isset($_POST['custname'])){ echo $due; } else echo 0;?></td>
                        </tr>
                        <tr>
                          <td><strong>Total:</strong></td>
                          <td><strong>Rs. <?php echo $amt ?></strong></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </section>
              </div>
            </article>
            <!-- /Data block -->
            
          </div>
          <!-- /Grid row -->
          
  </div>
  <?php
    unset($_POST['generate']);
    }
    else{
      header('Location: addtrans.php');
    }
  ?>
  <script src="js/pareek.js"></script>