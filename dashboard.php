<?php
  $page1 = "menu";
  $page = "dashboard";
  include "includes/header.php"; ?>


        <div class="span9">
          <div class="hero-unit">
            <?php
            include "db.php";
				    echo "<h1> Welcome  " .ucfirst($name).  "</h1>";
			     ?>
            <p> Easy view for all your transactions </p>
          </div>
          <div class="row-fluid">
            <div class="span4">
              <h4>Total Amount Due</h4>
              <blockquote>
              <p>
                <?php
                  $amount = mysql_query("SELECT sum(dueamount) FROM transaction") or die(mysql_error());
                  $values = mysql_fetch_array($amount);
                  echo "Rs. ".$values[0];

                ?>
              </p>
              <p id = "amtdue">   </p>
              </blockquote>
              <p><a class="btn" href="javascript:doAjaxShow('show=blah');">View details &raquo;</a></p>
            </div><!--/span-->
            <div class="span4">
              <h4>Settled Amount of this month</h4>
              <blockquote>
              <p>
                <?php
                  $settled = mysql_query("SELECT SUM( dueamount ) FROM  transaction WHERE dueamount <0") or die(mysql_error());
                  $stotal = mysql_fetch_array($settled);
                  echo "<p> Rs.  ".abs($stotal[0])."</p>";
                 
                ?>
              </p>
              <p id = "amtsettle"> </p>
              </blockquote>
              <p><a class="btn" href="javascript:doAjaxPrints('prints=blah');">View details &raquo;</a></p>
            </div><!--/span-->
            <div class="span4">
              <h4>Total number of customers</h4>
              <blockquote>
              <p>
              <?php
                $result = mysql_query("SELECT count(*),name FROM customers") or die(mysql_error());
                $val = mysql_fetch_array($result);
                echo $val[0]."<br/>";
              ?>
            </p>
            <p id = "totcust"> </p>
            </blockquote>
              <p><a class="btn" href="javascript:doAjaxDisplay('display=blah');">View details &raquo;</a></p>
            </div><!--/span-->
          </div><!--/row-->
          <!--div class="row-fluid">
            <div class="span4">
              <h2>Highest Owing Customer</h2>
              <p> </p>
              <p><a class="btn" href="#">View details &raquo;</a></p>
            </div>
            <div class="span4">
              <h2>Lowest Owing Customer</h2>
              <p></p>
              <p><a class="btn" href="#">View details &raquo;</a></p>
            </div>
          </div-->
        </div>


<?php include "includes/footer.php"; ?>
   <script type="text/javascript">
   $(document).ready(function() {
          $("#amtdue").hide();
          $("#amtsettle").hide();
          $("#totcust").hide();
      });

  var doAjaxShow = function(str){
            var url = "ajax/dashboard_details.php";
            url = url.concat("?");
            var newurl = url.concat(str);
            $.post(newurl, function(data){                
                $("#amtdue").html(data);
                $("#amtdue").slideToggle();              
            });
        };

  var doAjaxPrints = function(str){
            var url = "ajax/dashboard_details.php";
            url = url.concat("?");
            var newurl = url.concat(str);
            $.post(newurl, function(data){
                $("#amtsettle").html(data);
                $("#amtsettle").slideToggle();
            });
        };

  var doAjaxDisplay = function(str){
            var url = "ajax/dashboard_details.php";
            url = url.concat("?");
            var newurl = url.concat(str);
            $.post(newurl, function(data){
                $("#totcust").html(data);
                $("#totcust").slideToggle();
            });
        };
   </script>
