<?php
  $page1 = "menu";
  $page = "viewtrans";
  include "includes/header.php"; ?>
<div class="span9">
  <form class="form-horizontal" method="post" action="viewtrans.php">
  <fieldset>
    <legend>View Transaction</legend>
    <div class="control-group">
      <label class="control-label" for="inputEmail">Customer Name</label>
      <div class="controls">
        <input type="text" id="search" placeholder="Required" name="name" onClick="javascript:suggest(this,'customers','name');" required>
      </div>
    </div>
    <div class="control-group">
      <div class="controls">
        <button type="submit" class="btn" name="details">Submit</button>
      </div>
    </div>
  </fieldset>
</form>

<?php
  include "db.php";
  if(isset($_POST['details']) || isset($_GET['currentuser'])){
    if(isset($_POST['details'])){
      $name = ucfirst($_POST['name']);
      $result = mysql_query("SELECT ID FROM customers WHERE name = '$name'")or die(mysql_error());
      $row = mysql_fetch_array($result);
      $id = $row[0];
    }
    else if(isset($_GET['currentuser'])){
      $id = $_GET['currentuser'];
    }
    if($id == null){
?>
  <div class="control-group">  
      <label class="control-label" style="color:red"><strong>Customer not found. Make sure the customer is present.</strong></label>
  </div>
<?php
      }
      else{
        $result2 = mysql_query("SELECT * FROM transaction WHERE customerid=$id") or die(mysql_error());
?>
<table  class="table table-hover" id="trans-table">
  <tr>
    <td><button class="btn btn-primary" onClick="javascript:addtransaction(<?php echo $id ?>);"><i class="icon-plus-sign icon-white"></i> Add New </button></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr id="first">
    <th>#</th>
    <th>Date</th>
    <th>Details</th>
    <th>Total Amount</th>
    <th>Due Amount</th>
  </tr>
<?php
        $i = 0;        
        while($rows = mysql_fetch_array($result2)){
          $time = strtotime($rows[1]);
?> 
    <tr id="trans-<?php echo $rows[0] ?>">
    <td><?php echo $i+1; ?></td>
    <td><?php echo date("d-M-Y",$time); ?></td>
    <td class="edit-<?php echo $rows[0] ?>" id="details"><p id = "det-<?php echo $rows[0] ?>"> </p><a  href="javascript:doAjaxDisplay(<?php echo $rows[0]; ?>);">View Details</a></td>
    <td class="edit-<?php echo $rows[0] ?>" id="totalamount"><?php echo $rows[4];?></td>
    <td class="edit-<?php echo $rows[0] ?>" id="dueamount"><?php echo $rows[5]; ?></td>
    <!--td><button class="btn btn-success btn-small" onClick="javascript:edittrans(<?php echo $rows[0] ?>);"><i class="icon-pencil icon-white"></i></button></td-->
    <td><button class="btn btn-danger btn-small" onClick="javascript:deletetrans(<?php echo $rows[0]?>);"><i class="icon-remove icon-white"></i></button></td>
    </tr>
<?php 
    $i +=1;
  }
  $list = mysql_query("SELECT sum(dueamount) FROM transaction WHERE customerid=$id") or die(mysql_error());
  $val=mysql_fetch_array($list);
  $total = $val[0];
  //while($val=mysql_fetch_array($list)){
    //$total = $total + $val[0];
    //} 
?>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td><strong>Total Due</strong></td>
    <td><strong><?php echo $total; ?></strong></td>
    <td></td>
  </tr>
</table>
<?php
      }
  }
?>
</div> 
<?php include "includes/footer.php"; ?>
 <script type="text/javascript">

  function deletetrans(id) {
      if(confirm("Are you sure?")){
        //window.location = "viewtrans.php?del=" + id +"&customer="+ cname;
        var url = "ajax/delete_trans.php";
        $.post(url,{table: 'transaction' ,val : id },function(data){
          $("#trans-"+id).remove(); 
        });
      }  
  };

 function edittrans(id) {
       $(document).ready(function() {
        var url = "ajax/save_trans.php?edit=" + id;
     $('.edit-'+id).editable(url, { 
         id   : 'elementid',
         name : 'newvalue',
          indicator : 'Saving...',
         tooltip   : 'Click to edit...',
        callback: function(value, settings) {
        console.log(this);
        console.log(value);
        console.log(settings); 
      //window.location.reload();
        }   
       });
   });
 };
 function addtransaction(id) {
    $(document).ready(function() {
        var url = "ajax/add_trans.php?add=" + id;
        $('#first').after('<tr><td></td><td></td><td></td><td class="newrow" id="details"></td><td class="newrow" id="totalamount"></td><td class="newrow" id="dueamount"></td><td></td><td><button class="btn btn-success btn-small"><i class="icon-pencil icon-white"></i></button></td><td><button class="btn btn-danger btn-small"><i class="icon-remove icon-white"></i></button></td></tr>');
        $('.newrow').editable(url, {
            id   : 'elementid',
            name : 'newvalue',
            indicator : 'Saving...',
            tooltip   : 'Click to edit...',
            callback: function(value,settings){
              //console.log(value); 
              window.location = "viewtrans.php?currentuser=" + id;
            }
        });
    });
     // window.location = "addtrans.php";
 };
</script>   

<script type="text/javascript">
    
   
    var doAjaxDisplay = function(id){
            var url = "ajax/transaction_details.php";
            $.post(url,{ tid: id }, function(data){
              $('#det-'+id).html(data); 
              $('#det-'+id).slideToggle();   
            });
        };
</script>
   <script src="js/pareek.js"></script>
